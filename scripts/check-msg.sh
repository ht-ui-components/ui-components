#!/bin/bash
commit_msg=$(cat .git/COMMIT_EDITMSG);
$(dirname $0)/get-release-type.sh "$commit_msg" > /dev/null && exit 0 || ($(dirname $0)/display-format.sh; exit -1);
