#!/bin/bash

# Get release type
release=$($(dirname $0)/get-release-type.sh);

# Increase release depending on commit message (using Angular's syntax for release)
# Patch (Subminor) release
if [[ $release == 0 ]] ; then
  npm version prepatch --no-git-tag-version;
# Feature (Minor) release
elif [[ $release == 1 ]] ; then
  npm version preminor --no-git-tag-version;
# Breaking (Major) release
elif [[ $release == 2 ]] ; then
  npm version premajor --no-git-tag-version;
else
  $(dirname $0)/display-format.sh;
  exit -1;
fi
