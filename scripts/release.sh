#!/bin/bash

# Define main branch
main_branch="master";

# Avoid creating commit loops when ammending (which is necessary for increasing version)
if [ $SKIP_RELEASE ] ; then
  export SKIP_REPEASE=0;
  exit 0;
fi

# Determine the release type (main or develop) depending on current branch
current_branch=$(git rev-parse --abbrev-ref HEAD --);
if [ "$current_branch" == "$main_branch" ] ; then
  $(dirname $0)/main-release.sh;
else
  $(dirname $0)/develop-release.sh;
fi

if [ $? -ne 0 ] ; then
  exit -1;
fi

# Update version and modify present commit to include said change
git add -u package.json;
git add -u package-lock.json;
export SKIP_RELEASE=1;
git commit --amend --no-verify -C HEAD;
