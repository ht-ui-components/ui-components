import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class SegmentedProgressBar extends Vue {
  @Prop() private progress!: number;
  @Prop() private showBorder!: true;
  @Prop() private borderColor!: string;
  @Prop() private segmentColor!: string;
  @Prop() private enableBlinking!: boolean;
  @Prop() private segments!: number;
  @Prop() private segmentOffset!: number;
  @Prop() private height!: string;
  @Prop() private width!: string;
  @Prop() private borderRadius!: string;
  @Prop() private segmentBorderRadius!: string;
  @Prop() private fixedSegmentWidth!: number;

  private get segmentCount() : number {
    if (this.fixedSegmentWidth !== undefined) {
      return Math.trunc(100 / (this.fixedSegmentWidth + this.segmentMargin));
    }
    return this.segments || 10;
  }

  private get segmentWidth(): string {
    if (this.fixedSegmentWidth !== undefined) {
      return `${this.fixedSegmentWidth}%`;
    }
    const availableWidth = 100 - (this.segmentMargin * (this.segmentCount - 1));
    return `${availableWidth / this.segmentCount}%`;
  }

  private get segmentArray(): any {
    const array: any[] = [];
    for (let i = 0; i < this.segmentCount; i ++) {
      array.push({
        shouldBlink: this.shouldBlink(i),
      });
    }
    return array;
  }

  private get progressIndex(): number {
    return Math.trunc((this.progress || 0) * this.segmentCount);
  }

  private shouldBlink(index: number): boolean {
    if (!this.enableBlinking) {
      return false;
    }
    return index >= this.progressIndex && Math.abs(this.progressIndex - index) < 1;
  }

  private get segmentMargin(): number {
    // Default margin of 10%
    return this.segmentOffset || 2.5;
  }
}
