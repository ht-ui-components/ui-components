
# UI-Component-Set

**WARNING: this is a very early alpha version with limited components. More components will be added in the future.**

## Description

UI-Component-Set contains a set of Vue.js components written in Typescript, as well as a custom SCSS styling set of rules.

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Available components](#available-components)

## Installation

```sh
npm install ui-components
```

#### Prepare 'tsconfig.json'

Add "ui-component-set" to the "types" property. This will make Typescript UI-Component-Set type definitions, which is necessary for Vue.js components on this module to work properly.

## Usage

- SCSS styling can be imported using one of the following sintaxes

  ```typescript
  import 'ui-component-set/style/ui-components.scss';
  ```
  ```typescript
  require('ui-component-set/style/ui-components.scss');
  ```

NOTE:  Vue components are written on Typescript, so they should be imported into a Vue.js project which is also written on that language. Vue.js components on this package can be imported by using any of the following sintaxes:
  1. ### Import all components and then accesing specific components:

    ```typescript
    import UIComponentSet from 'ui-component-set';
    const <COMPONENT_NAME_1> = UIComponentSet.<COMPONENT_NAME_1>;
    const <COMPONENT_NAME_2> = UIComponentSet.<COMPONENT_NAME_2>;
    ...
    ```

  2. ### Import every specified component at once:

  ```typescript
    import { <COMPONENT_NAME_1>, <COMPONENT_NAME_2>, ... } from 'ui-component-set';
    ```


  3. ### Import single component at a time:

    ```typescript
    import <COMPONENT_NAME_1> from 'ui-component-set/<component-name_1>';
    import <COMPONENT_NAME_2> from 'ui-component-set/<component-name_2>';
    ...
    ```

## Available components

### Segmented Progress Bar (segmented-progress-bar)
| Property                    | Type    | Default | Description                                                                                                                                                                                                                                                                           |
|:----------------------------|:--------|:--------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| progress            | Number | 0   |  Progress expressed as a number between 0 and 1.
| showBorder      | Boolean  |    false     | Display border surrounding the progress bar.
| borderColor                    | String | #000   | Progress bar border color. **Has no effect if 'showBorder' is false or not specified'**                                                                                                                                                                                                                  |
| segmentColor           | String  | #000 | Set color for progress bar segments.                                                                              |
| enableBlinking          | Boolean | false  | If true, the segment next to the last active one will be blinking.                                                                                                                                                                                                            |
| segments             | Number  | 10    | Number of segments used to display the progress. **Has no effect if 'fixedSegmentWidth' is specified.**                                                                                                                                                                                                          |
| segmentOffset                  | Number | 2.5   | Define margin between segments.                                                                                                                                                                              |
| height                      | String  |  50px   | Define height of the progress bar. accepted.                                                                                                                                                                                                                    |
| width                        | String | 100%  |  Define width of the progress bar.                                                                                                                                                                         |
| borderRadius            | String  | 2.5px  | Define 'border-radius' property for progress bar's border. **Has no effect if 'showBorder' is false or not specified.**                                                                                                                                                                                     |
| segmentBorderRadius                   | String | 2.5px  | Define 'border-radius' property for the segments.                                                                                                                                                                                                     |
| fixedSegmentWidth                  | Number  |  undefined   | Define width for the segments. **If this property is specified, 'segments' will have no effect, as the number of segments will be calculated according to the 'fixedSegmentWidth' and 'segmentOffset' properties.**
