import Vue from 'vue';
import SegmentedProgressBar from './segmented-progress-bar/SegmentedProgressBar.vue';
import { WeblateService } from './weblate-service/weblate.service.ts';
import UIComponentStyle from './style/ui-components.scss';

export { SegmentedProgressBar, WeblateService, UIComponentStyle };

export default { SegmentedProgressBar, WeblateService, UIComponentStyle };
